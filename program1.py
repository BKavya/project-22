import tkinter as tk
from tkinter import messagebox
import sqlite3
import re

# Connect to SQLite database
conn = sqlite3.connect('users.db')
c = conn.cursor()

# Create table for registered users if it doesn't exist
c.execute('''CREATE TABLE IF NOT EXISTS users (
             username TEXT PRIMARY KEY,
             password TEXT,
             age INTEGER,
             phone_number TEXT
             )''')

def open_registration_window(previous_window):
    def register():
        entered_username = username_entry.get()
        entered_password = password_entry.get()
        entered_confirm_password = confirm_password_entry.get()
        entered_age = age_entry.get()
        entered_phone_number = phone_number_entry.get()

        if not entered_username or not entered_password or not entered_confirm_password or not entered_age or not entered_phone_number:
            messagebox.showerror("Error", "Please fill in all fields")
            return

        # Check if phone number is valid (exactly 10 digits)
        if not re.match(r"^\d{10}$", entered_phone_number):
            messagebox.showerror("Error", "Please enter a valid 10-digit phone number")
            return

        # Check if password is at least 6 characters long and contains both letters and numbers
        if not re.match(r"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$", entered_password):
            messagebox.showerror("Error", "Password must be at least 6 characters long and contain both letters and numbers")
            return

        if entered_password != entered_confirm_password:
            messagebox.showerror("Error", "Passwords do not match")
            return

        # Check if username already exists in the database
        c.execute("SELECT * FROM users WHERE username=?", (entered_username,))
        if c.fetchone() is not None:
            messagebox.showerror("Error", "Username already exists")
            return

        # Insert the new user into the database
        c.execute("INSERT INTO users VALUES (?, ?, ?, ?)", (entered_username, entered_password, entered_age, entered_phone_number))
        conn.commit()

        messagebox.showinfo("Registration Successful", "Registered successfully!")
        registration_window.destroy()
        previous_window.destroy()  # Close the previous window
        open_customer_options()  # Open customer options window after successful registration

    registration_window = tk.Toplevel()
    registration_window.title("Registration")
    registration_window.geometry("400x400")
    registration_window.configure(bg="#FFB6C1")

    username_label = tk.Label(registration_window, text="Enter Username:")
    username_label.pack(pady=5)

    username_entry = tk.Entry(registration_window)
    username_entry.pack(pady=5)

    password_label = tk.Label(registration_window, text="Enter Password:")
    password_label.pack(pady=5)

    password_entry = tk.Entry(registration_window, show="*")
    password_entry.pack(pady=5)

    confirm_password_label = tk.Label(registration_window, text="Confirm Password:")
    confirm_password_label.pack(pady=5)

    confirm_password_entry = tk.Entry(registration_window, show="*")
    confirm_password_entry.pack(pady=5)

    age_label = tk.Label(registration_window, text="Enter Age:")
    age_label.pack(pady=5)

    age_entry = tk.Entry(registration_window)
    age_entry.pack(pady=5)

    phone_number_label = tk.Label(registration_window, text="Enter Phone Number:")
    phone_number_label.pack(pady=5)

    phone_number_entry = tk.Entry(registration_window)
    phone_number_entry.pack(pady=5)

    register_button = tk.Button(registration_window, text="Register", command=register)
    register_button.pack(pady=5)

def open_login_window(login_success_callback):
    def login():
        entered_username = login_username_entry.get()
        entered_password = login_password_entry.get()

        c.execute("SELECT * FROM users WHERE username=? AND password=?", (entered_username, entered_password))
        user = c.fetchone()

        if user is not None:
            messagebox.showinfo("Login Successful", "Logged in successfully!")
            login_window.destroy()
            import program2m # Call the success callback function
        else:
            messagebox.showerror("Error", "Invalid username or password")

    login_window = tk.Toplevel()
    login_window.title("Login")
    login_window.geometry("200x200")
    login_window.configure(bg="#FFB6C1")

    login_username_label = tk.Label(login_window, text="Username:")
    login_username_label.pack(pady=5)

    login_username_entry = tk.Entry(login_window)
    login_username_entry.pack(pady=5)

    login_password_label = tk.Label(login_window, text="Password:")
    login_password_label.pack(pady=5)

    login_password_entry = tk.Entry(login_window, show="*")
    login_password_entry.pack(pady=5)

    login_button = tk.Button(login_window, text="Login", command=login)
    login_button.pack(pady=10)

def open_customer_options():
    def open_options_window():
        options_window = tk.Toplevel(root)  # Create a new Toplevel window
        options_window.title("Customer Options")
        options_window.geometry("300x200")
        options_window.configure(bg="#FFB6C1")

        def book_ticket():
            global ticket_counter
            ticket_counter += 1
            messagebox.showinfo("Book Ticket", f"Ticket booked successfully! Your unique ticket code is: {ticket_counter}")

        def cancel_ticket():
            # Add code to cancel ticket here
            messagebox.showinfo("Cancel Ticket", "Ticket canceled successfully!")

        def logout():
            options_window.destroy()
            root.deiconify()  # Restore the main window
            # Add any additional logout functionality here

        book_ticket_button = tk.Button(options_window, text="Book Ticket", command=book_ticket)
        book_ticket_button.pack(pady=10)

        cancel_ticket_button = tk.Button(options_window, text="Cancel Ticket", command=cancel_ticket)
        cancel_ticket_button.pack(pady=10)

        logout_button = tk.Button(options_window, text="Logout", command=logout)
        logout_button.pack(pady=10)

    def login_success():
        customer_options_window.withdraw()  # Hide the customer options window
        open_options_window()

    root.withdraw()  # Hide the main window
    customer_options_window = tk.Toplevel(root)
    customer_options_window.title("Customer Options")
    customer_options_window.geometry("300x200")
    customer_options_window.configure(bg="#FFB6C1")

    def open_registration():
        customer_options_window.withdraw()  # Hide the customer options window
        open_registration_window(customer_options_window)

    def open_login():
        customer_options_window.withdraw()  # Hide the customer options window
        open_login_window(login_success)

    def exit_application():
        root.destroy()  # Destroy the main window, thus exiting the application

    register_button = tk.Button(customer_options_window, text="Register", command=open_registration)
    register_button.pack(pady=10)

    login_button = tk.Button(customer_options_window, text="Login", command=open_login)
    login_button.pack(pady=10)

    exit_button = tk.Button(customer_options_window, text="Exit", command=exit_application)
    exit_button.pack(pady=10)

def open_admin_page():
    def show_seats_available(bus_number):
        # Get the number of seats available for the selected bus
        if bus_number == 1:
            seats_available = 40
        elif bus_number == 2:
            seats_available = 35
        elif bus_number == 3:
            seats_available = 45
        elif bus_number == 4:
            seats_available = 50
        else:
            seats_available = "Unknown"

        # Show a message with the seats available for the selected bus
        messagebox.showinfo(f"Bus {bus_number} Seats Available", f"Seats Available for Bus {bus_number}: {seats_available}")

    admin_window = tk.Toplevel(root)
    admin_window.title("Admin Page")
    admin_window.geometry("300x200")
    admin_window.configure(bg="#FFB6C1")

    label = tk.Label(admin_window, text="Select a Bus", bg="#FFB6C1")
    label.pack(pady=10)

    # Create buttons for each bus
    for bus_number in range(1, 5):
        bus_button = tk.Button(admin_window, text=f"Bus {bus_number}", command=lambda num=bus_number: show_seats_available(num))
        bus_button.pack(pady=5)

    def close_admin_window():
        admin_window.destroy()
        root.deiconify()  # Restore the main window

    close_button = tk.Button(admin_window, text="Close", command=close_admin_window)
    close_button.pack(pady=10)

def main():
    global root
    root = tk.Tk()
    root.title("Bus Ticket Booking System")
    root.geometry("300x200")
    root.configure(bg="#FFB6C1")

    option_frame = tk.Frame(root, bg="#FFB6C1")
    option_frame.pack(pady=20)

    label = tk.Label(option_frame, text="Select User Type", bg="#FFB6C1")
    label.pack()

    customer_button = tk.Button(option_frame, text="Customer", command=open_customer_options)
    customer_button.pack(pady=10)

    admin_button = tk.Button(option_frame, text="Admin", command=open_admin_page)
    admin_button.pack(pady=10)

    root.mainloop()

if __name__ == "__main__":
    main()

# Close the database connection after execution
conn.close()
